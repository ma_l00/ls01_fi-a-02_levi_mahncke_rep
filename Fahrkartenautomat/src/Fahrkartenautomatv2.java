
import java.util.Scanner;

class Fahrkartenautomatv2{
    
  public static void main(String[] args){
		
  char abschluss;
  double zuZahlenderBetrag;    
	
  do {
	  
    do {
    	  
        zuZahlenderBetrag = wasMussIchZahlen();
        
    } while (zuZahlenderBetrag == 0);
        
        double eingezahlterGesamtbetrag = geldEinwerfen(zuZahlenderBetrag);
       
        fahrscheinAusgeben();
       
        berechneR�ckgeld(eingezahlterGesamtbetrag, zuZahlenderBetrag);
      
        abschluss = verkaufAbschlie�en();
       
       } 
	
	while (abschluss == 'j');
      
    }

	static double wasMussIchZahlen(){
		 char jaNein; 		
		 int anzahlDerFahrkarten;
		 double zuZahlen;
		 Scanner tastatur = new Scanner(System.in);
		 
		 System.out.print("Zu zahlender Betrag (EURO): ");
	     zuZahlen = tastatur.nextDouble();
	     System.out.print("Anzahl der Fahrkarten: ");
	     anzahlDerFahrkarten = tastatur.nextInt();
	    
	     if (anzahlDerFahrkarten < 0 || anzahlDerFahrkarten > 10 ) {
	     System.out.println("FEHLER!\n");
	     System.out.println("Die ausgew�hlte Anzahl der Tickets ist leider ung�ltig!\nEs wird mit Ticketanzahl 1 fortgefahren!\n\n");
	     System.out.println("Abbruch? [j/n]\n");
	     jaNein = tastatur.next().charAt(0);
	     
	     if (jaNein == 'j') {
	     System.out.println("Auf Wiedersehen!\n");
    	 zuZahlen = 0;
    	 return zuZahlen;
	     }
	     anzahlDerFahrkarten = 1;
	     zuZahlen = zuZahlen * anzahlDerFahrkarten;
	     return zuZahlen;
	     
	     } else {
	    	 
	     zuZahlen = zuZahlen * anzahlDerFahrkarten;
		 return zuZahlen; 
		 
	     }
	  }
		
	static double geldEinwerfen(double zuZahlen){
		double eingeworfeneM�nze;
		double gesamtBetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);
		while(gesamtBetrag < zuZahlen)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f%n",  (zuZahlen - gesamtBetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           gesamtBetrag += eingeworfeneM�nze;
	       }
		return gesamtBetrag;
		}
	
	static void fahrscheinAusgeben() {
		
		  System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
		
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	static void berechneR�ckgeld(double gesamtBetrag, double zuZahlen){
		double r�ckgabebetrag;
		r�ckgabebetrag = gesamtBetrag - zuZahlen;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f%n",  r�ckgabebetrag, " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:\n");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	 	         
	 	       
	           }
	          
	}
		
}

	static char verkaufAbschlie�en() {
		char jaNein;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir w�nschen Ihnen eine gute Fahrt.\n");

		System.out.println("========\n");
		System.out.println("Weitere Tickets kaufen? [j/n]");
		jaNein = tastatur.next().charAt(0);
		
		return jaNein;
	}
	
}
